import sys

from openpyxl import load_workbook
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


# Delayer function
def delayer(seconds, by):
    delay = seconds
    try:
        WebDriverWait(driver, delay).until(EC.presence_of_element_located(by))
        print("Delayer: The page is ready")
    except TimeoutException:
        print("Delayer: Loading took too much time")
        sys.exit()


def wait(seconds):
    delay = seconds
    WebDriverWait(driver, delay)


# Set the driver
driver = webdriver.Firefox()

# Load the workbook
my_workbook = load_workbook('Python_Excel.xlsx')

# Set the worksheet
my_worksheet = my_workbook.active
print(my_worksheet)

# Get the values
user = my_worksheet['B1'].value
print(user)
password = my_worksheet['D2'].value
print(password)

# Webpage to navigate to:
i = 1
while i == 1:
    driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account")
    wait(10)
    try:
        driver.find_element_by_id('email')
        i = 0
    except:
        print("Could not load the page... retrying in 15 seconds")
        wait(15)
        i = 1

# All set, its showtime! -------------------------------------------------------

print("Entering the e-mail...")
driver.find_element_by_id('email').send_keys(user)

print("Entering the password...")
driver.find_element_by_id('passwd').send_keys(password)

print(" Click the [Sign in] button...")
driver.find_element_by_id('SubmitLogin').click()

print("Redirecting to the account welcoming page")
delayer(30, (By.CLASS_NAME, 'logout'))

if driver.find_element_by_class_name('logout').is_displayed():
    print("Test completed")

# All done --------------------------------------------------------

#driver.quit()  # Close browser
quit()
sys.exit()
