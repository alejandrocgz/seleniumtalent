import sys

from openpyxl import load_workbook
from selenium import webdriver

# Set the driver
driver = webdriver.Firefox()

# Load the workbook
my_workbook = load_workbook('02_ExcelWebTable.xlsx')

# Set the worksheet
my_worksheet = my_workbook.active
print(my_worksheet)

# Webpage to navigate to:
driver.get("https://cosmocode.io/automation-practice-webtable")

# All set, its showtime! -------------------------------------------------------

for y in range(197):
    columns = ['A', 'B', 'C', 'D', 'E']
    n = 1
    for x in columns:
        my_worksheet['{}{}'.format(x, y + 1)].value = driver.find_element_by_xpath(
            '//*[@id="countries"]/tbody/tr[{}]/td[{}]'.format(y + 1, n)).text
        n = n + 1

my_workbook.save('02_ExcelWebTable.xlsx')

print("Finished")

# All done --------------------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
