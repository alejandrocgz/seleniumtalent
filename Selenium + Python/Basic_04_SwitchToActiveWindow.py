import sys

from selenium import webdriver

# Set the driver
driver = webdriver.Firefox()

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/switch-window")

# All set, its showtime! ----------------------------

# Save the ID of the original window
original_window = driver.current_window_handle

# Open a new tab window
newTabButton = driver.find_element_by_id("new-tab-button")
newTabButton.click()

# Loop through until we find a new window handle
for window_handle in driver.window_handles:
    if window_handle != original_window:
        driver.switch_to.window(window_handle)
        break

# Close the window
driver.close()

# Switch to the original window
driver.switch_to.window(original_window)

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
