import sys

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

# Set the driver
driver = webdriver.Firefox()
actions = ActionChains(driver)

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/radiobutton")

# All set, its showtime! ----------------------------

radioButton1 = driver.find_element_by_id("radio-button-1")
radioButton1.click()

radioButton2 = driver.find_element_by_css_selector("input[value='option2']")
radioButton2.click()

radioButton3 = driver.find_element_by_xpath("/html/body/div/div[3]/input")
radioButton3.click()

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
