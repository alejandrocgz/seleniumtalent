import sys

from selenium import webdriver

# Set the driver
driver = webdriver.Firefox()

# Webpage to navigate to:
driver.get("https://cosmocode.io/automation-practice-webtable")

# All set, its showtime! -------------------------------------------------------

driver.find_element_by_css_selector('body').screenshot('Basic_13_FullPage.png')
driver.find_element_by_id('countries').screenshot('Basic_13_CountriesTable.png')
print("Finished")

# All done --------------------------------------------------------

driver.quit()  # Close browser
quit()
sys.exit()
