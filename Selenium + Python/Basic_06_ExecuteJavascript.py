import sys

from selenium import webdriver

# Set the driver
driver = webdriver.Firefox()

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/modal")

# All set, its showtime! ----------------------------

modalButton = driver.find_element_by_id("modal-button")
modalButton.click()

closeButton = driver.find_element_by_id("close-button")

# Executes JavaScript code snippet in the current context of a selected frame or window.
driver.execute_script("arguments[0].click();", closeButton)

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
