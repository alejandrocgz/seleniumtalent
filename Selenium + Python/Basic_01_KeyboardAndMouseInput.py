import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

# Select the browser
driver = webdriver.Firefox()

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/keypress")

# Wait for element id "name" to show. This is not necessary since the drive.get method
# makes sure the page is fully loaded before advancing to the next line in the code.
timeout = 5
try:
    element_present = EC.presence_of_element_located((By.ID, 'name'))
    WebDriverWait(driver, timeout).until(element_present)
except TimeoutException:
    print("Timed out waiting for page to load")

# All set, its showtime! ----------------------------
name = driver.find_element_by_id("name")
name.click()
name.send_keys("Alejandro Chavez")
print("Name entered")

button = driver.find_element_by_id("button")
button.click()
print("Button clicked")

# All done -------------------------------------------
driver.quit()  # Close browser

quit()
sys.exit()
