import sys

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

# Set the driver
driver = webdriver.Firefox()
actions = ActionChains(driver)

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/dragdrop")

# All set, its showtime! ----------------------------

image = driver.find_element_by_id("image")

box = driver.find_element_by_id("box")

actions.drag_and_drop(image, box).perform()

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
