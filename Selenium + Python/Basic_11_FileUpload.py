import sys

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

# Set the driver
driver = webdriver.Firefox()
actions = ActionChains(driver)

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/fileupload")

# All set, its showtime! ----------------------------

fileUploadField = driver.find_element_by_id("file-upload-field")
fileUploadField.send_keys("HTML_Exercise.html")

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
