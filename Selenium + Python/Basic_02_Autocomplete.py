import sys

from selenium import webdriver

# Select the browser
driver = webdriver.Firefox()

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/autocomplete")

# All set, its showtime! ----------------------------
autocomplete = driver.find_element_by_id("autocomplete")
autocomplete.send_keys("Francisco I. Madero 8 Zona Centro, San Cristobal de las Casas, Chiapas")

autocompleteResult = driver.find_element_by_class_name("pac-container")
autocompleteResult.click()

# All done -------------------------------------------

#driver.quit()  # Close browser
quit()
sys.exit()
