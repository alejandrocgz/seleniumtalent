import sys

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

# Set the driver
driver = webdriver.Firefox()
actions = ActionChains(driver)

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/dropdown")

# All set, its showtime! ----------------------------

dropDownMenu = driver.find_element_by_id("dropdownMenuButton")
dropDownMenu.click()

autocompleteItem = driver.find_element_by_css_selector("div.show:nth-child(2) > a:nth-child(13)")
autocompleteItem.click()

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
