import sys

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

# Set the driver
driver = webdriver.Firefox()
actions = ActionChains(driver)

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/datepicker")

# All set, its showtime! ----------------------------

dateField = driver.find_element_by_id("datepicker")
dateField.send_keys("09/09/2021")
dateField.send_keys(Keys.RETURN)

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
