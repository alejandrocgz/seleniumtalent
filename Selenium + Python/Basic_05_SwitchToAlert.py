import sys

from selenium import webdriver

# Set the driver
driver = webdriver.Firefox()

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/switch-window")

# All set, its showtime! ----------------------------

# Click the button that displays the alert
alertButton = driver.find_element_by_id("alert-button")
alertButton.click()

# Focus that alert and accept it
alert = driver.switch_to.alert
alert.accept()

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
