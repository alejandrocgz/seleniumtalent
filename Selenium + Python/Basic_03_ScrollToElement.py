import sys

from selenium import webdriver

# Set the driver
driver = webdriver.Firefox()

# Webpage to navigate to:
driver.get("https://formy-project.herokuapp.com/scroll")

# All set, its showtime! ----------------------------
name = driver.find_element_by_id("name")

driver.execute_script("arguments[0].scrollIntoView()", name)

name.send_keys("Alejandro")
date = driver.find_element_by_id("date")
date.send_keys("08/09/2021")

# All done -------------------------------------------

# driver.quit()  # Close browser
quit()
sys.exit()
