/*
 *   Author: Alejandro Chavez Gonzalez
 *   ID: 2045348
 *
 *   Date: September 3rd, 2021
 * */

/*
 *   To exemplify Ex 2. Inheritance I have created this class that extends the Character class.
 *   The player is a character inside the game, so it has a name, species, health points, and the
 *   methods set in the Character class which are required to read and modify them.
 *
 *   However, since the player is a special character it must be able to regain health or do some
 *   other special functions. So I have added the healHP method.
 * */

public class Player extends Character {

    public Player(String enteredName, int selection) {
        super(enteredName, selection);
    }

    public void healHP(int points) {
        this.healthPoints += points;
    }


    /*
     *   Finally my example of Polymorphism using an Override.
     *   Because the attack must match the player's species.
     * */

    @Override
    public void specialAttack() {
        switch (this.species) {
            case 1:
                System.out.println("Peck!");
                break;
            case 2:
                System.out.println("Bite!");
                break;
            case 3:
                System.out.println("Scratch!");
                break;
            default:
                break;
        }
    }


}
