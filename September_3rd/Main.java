/*
 *   Author: Alejandro Chavez Gonzalez
 *   ID: 2045348
 *
 *   Date: September 3rd, 2021
 * */

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    /*
     *   Hello Mr. Kumar.
     *   This was the most fun way of practicing the polymorphism and inheritance techniques I could think of.
     *   It's a little game about a battle between an animal (The Player), and a Human named Bob (NPC).
     *
     *   Ex. 1: How does Java implement polymorphism? Give an example.
     *   The polymorphism in Java can be implemented using Override and Overload.
     *   The Override technique is used when an inherited class has a method that exists in the superclass,
     *   but needs to be redefined in the subclass.
     *
     *   The Overload technique is used when you want to have different methods, with different parameter inputs,
     *   but with the same name.
     *
     *       An example can be found in the class Player line 27.
     *
     *   Ex. 2: How does Java implement inheritance? Give an example.
     *   Inheritance is implemented using the keyword "extends", followed by the name of the superclass.
     *   In that moment the subclass acquires its own copy of the variables and methods defined in the
     *   superclass.
     *
     *       An example is the Player subclass that inherits the "traits" of the Character superclass.
     *
     *   Ex. 3: What is try... catch in Java? Give an example.
     *   This is Java's error handling method, where the code which execution will be attempted goes
     *   inside the "try" area; if an exception is risen, the code inside the "catch" area will be
     *   executed instead.
     *
     *       An example can be found at line 67 inside this Main class.
     */


    public static void main(String[] arg) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("STARTING NEW GAME...\n");

        System.out.println("Enter the name of your character.:");
        String enteredName = scanner.nextLine();


        System.out.println("Select your species:\n");
        System.out.println("\t1. Avian");
        System.out.println("\t2. Canine");
        System.out.println("\t3. Feline");

        String selection;
        int intSelection = 1;

        int run = 1;
        do {

            /*  To exemplify Ex. 3: The use of try... catch, I am asking for a number in the next lines
             *  and if the user writes a non numeric character the program would return an error, but I
             *  do not want the program to stop executing, so I use a try...catch to handle the error
             *  and request the user to try again.
             * */

            selection = scanner.next();

            try {
                intSelection = Integer.parseInt(selection);
                run = 0;
            } catch (Exception e) {
                System.out.println("Please select a numeric option: 1, 2, or 3.");
            }
        } while (run == 1);

        //Instantiating the characters...

        Character enemy = new Character("Bob", 0);
        Player player = new Player(enteredName, intSelection);


        System.out.println("You are a(n) " + player.getSpecies() + " named " + player.getName());
        System.out.println("You are facing: " + enemy.getName() + ", the " + enemy.getSpecies());

        //All set to start the fight...

        byte ongoingFight = 1;
        do {

            System.out.print("\n" + player.getName() + ":");
            System.out.print("\tHP: " + player.getHealthPoints() + "/" + player.getTotalHP());

            System.out.print("\n" + enemy.getName() + ":");
            System.out.print("\tHP: " + enemy.getHealthPoints() + "/" + enemy.getTotalHP());


            //Player's turn
            System.out.println("\n\nWhat's your next move?");
            System.out.println(" 1. Melee attack");
            System.out.println(" 2. Special attack");
            System.out.println(" 3. Heal up");
            int nextMove = scanner.nextInt();

            switch (nextMove) {
                case 1 -> {
                    int randomMelee = ThreadLocalRandom.current().nextInt(18, 25);
                    enemy.receiveDamage(randomMelee);
                    System.out.println("\n" + player.getName() + " attacks, dealing " + randomMelee + " damage points");
                }
                case 2 -> {
                    int randomSpecial = ThreadLocalRandom.current().nextInt(30, 40);
                    enemy.receiveDamage(randomSpecial);
                    player.specialAttack();
                    System.out.println("\n" + player.getName() + " used their special attack, dealing " + randomSpecial + " damage points");
                }
                case 3 -> {
                    int randomHeal = ThreadLocalRandom.current().nextInt(15, 25);
                    player.healHP(randomHeal);
                    System.out.println("\n" + player.getName() + " heals up " + randomHeal + " points");
                }
                default -> System.out.println("You decided to wait.");
            }

            //Enemy's turn
            int randomEnemyAttack = ThreadLocalRandom.current().nextInt(18, 25);
            player.receiveDamage(randomEnemyAttack);
            System.out.println("\n" + enemy.getName() + " attacks, dealing " + randomEnemyAttack + " damage points");


            //Check on the HP of the characters to see if the fight is over.
            if (player.getHealthPoints() <= 0) {
                System.out.println("\n" + player.getName() + " has fainted.");
                System.out.println("YOU LOSE!");
                ongoingFight = 0;
            }
            if (enemy.getHealthPoints() <= 0) {
                System.out.println("\n" + enemy.getName() + " has fainted.");
                System.out.println("YOU WIN!");
                ongoingFight = 0;
            }

            //Fix the players HP in case it goes over MaxHP
            if (player.getHealthPoints() > player.getTotalHP()) {
                player.healthPoints = player.getTotalHP();
            }

        } while (ongoingFight == 1);
    }
}
