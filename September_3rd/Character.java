/*
 *   Author: Alejandro Chavez Gonzalez
 *   ID: 2045348
 *
 *   Date: September 3rd, 2021
 * */

public class Character {

    protected String name;
    protected int species = 0;
    protected int healthPoints = 100;
    protected int totalHP = 100;

    //THE BUILDER

    public Character(String name, int species) {
        this.name = name;
        this.species = species;
    }

    /*This next method has to be changed according to the species. The default special attack is Kick because
     *  the enemy is always a human.
     *  Therefore I must override it in the Player subclass,
     *  so the move matches the player's selected species.
     *
     *  I do so in the Player subclass, on line 27.
     */

    public void specialAttack() {
        System.out.println("Kick!");
    }

    public void setSpecies(int speciesInput) {
        species = speciesInput;
    }

    public String getSpecies() {
        String currentSpecies = "Human";
        switch (species) {
            case 0:
                currentSpecies = "Human";
                break;
            case 1:
                currentSpecies = "Avian";
                break;
            case 2:
                currentSpecies = "Canine";
                break;
            case 3:
                currentSpecies = "Feline";
                break;
            default:
                break;
        }
        return currentSpecies;
    }

    public void receiveDamage(int damagePoints) {
        healthPoints -= damagePoints;
    }

    public void setName(String nameInput) {
        name = nameInput;
    }

    public String getName() {
        return name;
    }

    public int getTotalHP() {
        return totalHP;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

}
