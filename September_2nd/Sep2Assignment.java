/*
 *   Author: Alejandro Chavez Gonzalez
 *   ID: 2045348
 *
 *   Date: September 2nd, 2021
 * */

import org.jetbrains.annotations.NotNull;

public class Sep2Assignment {

    /*Ex. 1: Write a Java program to break an integer into a sequence of individual digits. */
    public void intDisassembler(int number) {
        char[] sequence;
        String seq = String.valueOf(number);
        sequence = seq.toCharArray();
        for (int i = 0; i < sequence.length; i++) {
            if (i == sequence.length - 1) {
                System.out.print(sequence[i]);
            } else {
                System.out.print(sequence[i] + ", ");
            }
        }
    }

    /*Ex. 2: Write a Java program to count the letters, spaces, numbers and other characters of an input string*/
    public void countInString(String input) {

        char[] convertedInput = input.toCharArray();

        //System.out.println(convertedInput); For debugging

        int amountOfLetters = 0;
        int amountOfNumbers = 0;
        int amountOfSpaces = 0;
        int amountOfOther = 0;

        for (int i = 0; i < convertedInput.length; i++) {

            if (Character.isLetter(convertedInput[i])) {
                amountOfLetters++;
            } else if (Character.isDigit(convertedInput[i])) {
                amountOfNumbers++;
            } else if (Character.isSpaceChar(convertedInput[i])) {
                amountOfSpaces++;
            } else {
                amountOfOther++;
            }
        }

        System.out.println("\nLetters: " + amountOfLetters);
        System.out.println("Numbers: " + amountOfNumbers);
        System.out.println("Spaces: " + amountOfSpaces);
        System.out.println("Other: " + amountOfOther);
    }

    /*Ex. 3: Find the 3 largest elements in a given array. The elements in the array can be in any order*/
    public void largestElements(int amount, @NotNull String input) {

        char[] convertedInput = input.toCharArray();
        StringBuilder temporal = new StringBuilder();

        //Transform input into only numbers
        int j = 0;
        for (int i = 0; i < convertedInput.length; i++) {

            if (Character.isDigit(convertedInput[i])) {
                temporal.append(convertedInput[i]);
                j++;
            }
        }

        System.out.println("Numbers entered: " + temporal);


        char[] array = temporal.toString().toCharArray();

        char temporalValue;


        for (int i = 0; i < array.length; i++) {

            for (int k = i + 1; k < array.length; k++) {

                if (array[i] < array[k]) {
                    temporalValue = array[i];
                    array[i] = array[k];
                    array[k] = temporalValue;
                }
            }
        }

        System.out.println("The ordered array is: ");
        printArray(array);

        System.out.println("The biggest " + amount + " numbers are: ");

        for (int i = 0; i < amount; i++) {
            if (i == amount - 1) {
                System.out.print(array[i] + "\n");
            } else {
                System.out.print(array[i] + ", ");
            }
        }
    }


    public void printArray(char[] array) {

        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                System.out.print(array[i] + "\n");
            } else {
                System.out.print(array[i] + ", ");
            }
        }
    }
}
