/*
*   Author: Alejandro Chavez Gonzalez
*   ID: 2045348
*
*   Date: September 2nd, 2021
* */

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Sep2Assignment Sep2A = new Sep2Assignment(); //Instantiated

        byte active = 1;

        do {
            System.out.println("\n******HELLO USER!******\n");
            System.out.println("What's it gonna be?\n");
            System.out.println("\t1. Break an integer into a sequence of individual digits");
            System.out.println("\t2. Count the letters, spaces, numbers and other characters of an input string");
            System.out.println("\t3. Find the 3 largest elements in a given array.");


            Scanner scanner = new Scanner(System.in);
            int selectedOption = scanner.nextInt();

            switch (selectedOption) {
                case 1:
                    System.out.println("Enter the number:");
                    int enteredNumber = scanner.nextInt();
                    Sep2A.intDisassembler(enteredNumber);
                    break;
                case 2:
                    System.out.println("Enter the string:");
                    String skipLine = scanner.nextLine(); //So that the next line works...
                    String enteredString = scanner.nextLine();
                    Sep2A.countInString(enteredString);
                    break;
                case 3:
                    System.out.println("Enter the array in the format of a number ('123456789')");
                    skipLine = scanner.nextLine(); //So that the next line works...
                    enteredString = scanner.nextLine();
                    Sep2A.largestElements(3, enteredString);
                    break;
                default:
                    active = 0;
            }
        } while (active == 1);
    }
}
