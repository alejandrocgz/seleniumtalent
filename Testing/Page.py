from Locators import MainPage_Locators, SignInPage_Locators, AccountCreationPage_Locators, CartPage_Locators


class BasePage:
    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):

    def click_login_button(self):
        self.driver.find_element(*MainPage_Locators.SIGN_IN_BUTTON).click()

    def login_button_visible(self):
        return self.driver.find_element(*MainPage_Locators.SIGN_IN_BUTTON).is_displayed()

    def logout_button_visible(self):
        return self.driver.find_element(*MainPage_Locators.LOGOUT_BUTTON).is_displayed()

    def blouse27_addToCart(self):
        self.driver.get('http://automationpractice.com/index.php?controller=cart&add=1&id_product=2&token'
                        '=e817bb0705dd58da8db074c69f729fd8')

    def pSummerDress28_addToCart(self):
        self.driver.get('http://automationpractice.com/index.php?controller=cart&add=1&id_product=5&token'
                        '=e817bb0705dd58da8db074c69f729fd8')

    def pChiffonDress16_addToCart(self):
        self.driver.get('http://automationpractice.com/index.php?controller=cart&add=1&id_product=7&token'
                        '=e817bb0705dd58da8db074c69f729fd8')


class LogInSignInPage(BasePage):
    def new_account_email(self, email):
        self.driver.find_element(*SignInPage_Locators.EMAIL_CREATE_INPUT_FIELD).send_keys(email)

    def click_create_account(self):
        self.driver.find_element(*SignInPage_Locators.CREATE_ACCOUNT_BTN).click()

    def enter_email(self, email):
        self.driver.find_element(*SignInPage_Locators.EMAIL_INPUT_FIELD).send_keys(email)

    def enter_password(self, password):
        self.driver.find_element(*SignInPage_Locators.PASSWORD_INPUT_FIELD).send_keys(password)

    def click_sign_in_button(self):
        self.driver.find_element(*SignInPage_Locators.SIGN_IN_BUTTON).click()


class AccountCreationPage(BasePage):
    def select_Mr_radiobutton(self):
        self.driver.find_element(*AccountCreationPage_Locators.MR_RADIOBUTTON).click()

    def select_Mrs_radiobutton(self):
        self.driver.find_element(*AccountCreationPage_Locators.MRS_RADIOBUTTON).click()

    def enter_firstname(self, firstname):
        self.driver.find_element(*AccountCreationPage_Locators.FIRSTNAME).send_keys(firstname)

    def enter_lastname(self, lastname):
        self.driver.find_element(*AccountCreationPage_Locators.LASTNAME).send_keys(lastname)

    def enter_password(self, password):
        self.driver.find_element(*AccountCreationPage_Locators.PASSWORD).send_keys(password)

    def enter_birth_day(self):
        self.driver.find_element(*AccountCreationPage_Locators.BIRTH_DAY).click()

    def enter_birth_month(self):
        self.driver.find_element(*AccountCreationPage_Locators.BIRTH_MONTH).click()

    def enter_birth_year(self):
        self.driver.find_element(*AccountCreationPage_Locators.BIRTH_YEAR).click()

    def subscribe_to_newsletter(self):
        self.driver.find_element(*AccountCreationPage_Locators.NEWSLETTER_SUB).click()

    def enter_address(self, address):
        self.driver.find_element(*AccountCreationPage_Locators.ADDRESS).send_keys(address)

    def enter_city(self, city):
        self.driver.find_element(*AccountCreationPage_Locators.CITY).send_keys(city)

    def select_country(self):
        self.driver.find_element(*AccountCreationPage_Locators.COUNTRY).click()

    def select_state(self):
        self.driver.find_element(*AccountCreationPage_Locators.STATE).click()

    def enter_postcode(self, postcode):
        self.driver.find_element(*AccountCreationPage_Locators.POSTCODE).send_keys(postcode)

    def enter_cellphone(self, cellphone):
        self.driver.find_element(*AccountCreationPage_Locators.CELLPHONE).send_keys(cellphone)

    def enter_alias(self, alias):
        self.driver.find_element(*AccountCreationPage_Locators.ALIAS).send_keys(alias)

    def click_submit_button(self):
        self.driver.find_element(*AccountCreationPage_Locators.SUBMIT_BUTTON).click()


class CartPage(BasePage):

    def click_continue_shopping(self):
        self.driver.find_element(*CartPage_Locators.CONTINUE_SHOPPING_BUTTON).click()

    def remove_Blouse27(self):
        self.driver.find_element(*CartPage_Locators.REMOVE_BLOUSE27).click()

    def get_total(self):
        return self.driver.find_element(*CartPage_Locators.TOTAL).text

    def click_proceed(self):
        self.driver.find_element(*CartPage_Locators.PROCEED_CHECKOUT).click()

    def mark_delivery_as_billing_address(self):
        self.driver.find_element(*CartPage_Locators.CHECKBOX_DELIVERY_AS_BILLING_ADDRESS)

    def un_mark_delivery_as_billing_address(self):
        self.driver.find_element(*CartPage_Locators.CHECKBOX_DELIVERY_AS_BILLING_ADDRESS).click()

    def click_process_address_btn(self):
        self.driver.find_element(*CartPage_Locators.PROCESS_ADDRESS_BUTTON).click()

    def mark_accept_ToS(self):
        self.driver.find_element(*CartPage_Locators.CHECKBOX_TOS).click()

    def click_process_carrier_btn(self):
        self.driver.find_element(*CartPage_Locators.PROCESS_CARRIER_BUTTON).click()

    def click_pay_by_check_btn(self):
        self.driver.find_element(*CartPage_Locators.PAY_BY_CHECK).click()

    def click_confirm_order_btn(self):
        self.driver.find_element(*CartPage_Locators.CONFIRM_ORDER_BUTTON).click()

    def was_successful(self):
        return self.driver.find_element(*CartPage_Locators.SUCCESS_ALERT).is_displayed()

    def fancybox_error_displayed(self):
        return self.driver.find_element(*CartPage_Locators.ToS_ERROR).is_displayed()