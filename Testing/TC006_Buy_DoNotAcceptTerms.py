import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import Page
import time


# Define a delayer function
def delayer(seconds, by):
    delay = seconds
    try:
        WebDriverWait(driver, delay).until(EC.presence_of_element_located(by))
        print("Delayer: The page is ready")
    except TimeoutException:
        print("Delayer: Loading took too much time")
        sys.exit()


def wait(seconds):
    delay = seconds
    WebDriverWait(driver, delay)


# Set the driver
driver = webdriver.Firefox()

# Required Page Objects
main_page = Page.MainPage(driver)
sign_in_page = Page.LogInSignInPage(driver)
cart_page = Page.CartPage(driver)

print("PRECONDITIONS: Adding 1 item to the cart")

i = 1
while i == 1:
    driver.get("http://automationpractice.com/index.php")
    wait(10)
    try:
        main_page.login_button_visible()
        i = 0
    except:
        print("Could not load the page... retrying in 15 seconds")
        wait(15)
        i = 1

print("Add the Printed Summer Dress ($28.98) to he cart...")
main_page.pSummerDress28_addToCart()
time.sleep(10)

print("Reading the total value...")
total = cart_page.get_total()
print("Value: ", total)

if total == "$30.98":
    print("The total equals $30.98")
else:
    print("Something went wrong")
    sys.exit()

print("PRECONDITIONS: Signing in")
i = 1
while i == 1:
    driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account")
    wait(10)
    try:
        print("Sign up")
        sign_in_page.enter_email("someone@test.com")
        sign_in_page.enter_password("mypass123")

        sign_in_page.click_sign_in_button()
        i = 0
    except:
        print("Could not load the page... retrying in 15 seconds")
        wait(15)
        i = 1

print("PRECONDITIONS: Navigating to cart page")

i = 1
while i == 1:
    driver.get("http://automationpractice.com/index.php?controller=order")
    wait(10)
    try:
        print("Proceed to checkout")
        cart_page.click_proceed()
        i = 0
    except:
        print("Could not load the page... retrying in 15 seconds")
        wait(15)
        i = 1

print("PRECONDITIONS MET. STARTING...")

# All set, its showtime! -------------------------------------------------------

print('Address process page')
cart_page.click_process_address_btn()

print('Shipping process page')
cart_page.click_process_carrier_btn()

print('Validating ToS Error pop-up...')
if cart_page.fancybox_error_displayed() is True:
    print('Error message correctly displayed')
    print('Test completed')
else:
    print('Something went wrong')

# All done --------------------------------------------------------

driver.quit()  # Close browser
quit()
sys.exit()
