import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import Page
from Locators import SignInPage_Locators, AccountCreationPage_Locators


# Define a delayer function
def delayer(seconds, by):
    delay = seconds
    try:
        WebDriverWait(driver, delay).until(EC.presence_of_element_located(by))
        print("Delayer: The page is ready")
    except TimeoutException:
        print("Delayer: Loading took too much time")
        sys.exit()


def wait(seconds):
    delay = seconds
    WebDriverWait(driver, delay)


# Set the driver
driver = webdriver.Firefox()

# Required Page Objects
main_page = Page.MainPage(driver)
sign_in_page = Page.LogInSignInPage(driver)
account_create_page = Page.AccountCreationPage(driver)

# Webpage to navigate to:
i = 1
while i == 1:
    driver.get("http://automationpractice.com/index.php")
    wait(10)
    try:
        main_page.click_login_button()
        i = 0
    except:
        print("Could not load the page... retrying in 15 seconds")
        wait(15)
        i = 1

# All set, its showtime! -------------------------------------------------------

print("Redirecting to the login/sign in page...")
delayer(30, SignInPage_Locators.EMAIL_CREATE_INPUT_FIELD)

print("Entering the e-mail...")
sign_in_page.new_account_email("someone1202mosdawm@test.com")

print("Click the [Create an account] button...")
sign_in_page.click_create_account()

print("Redirecting to the account creation page...")
delayer(30, AccountCreationPage_Locators.MRS_RADIOBUTTON)

print("Entering information...")
account_create_page.select_Mrs_radiobutton()
account_create_page.enter_firstname("Some256488%")
account_create_page.enter_lastname("Else")
account_create_page.enter_password("password123")
account_create_page.enter_birth_day()
account_create_page.enter_birth_month()
account_create_page.enter_birth_year()
account_create_page.subscribe_to_newsletter()
account_create_page.enter_address("%%/*ss23")
account_create_page.enter_city("Gotham")
account_create_page.select_country()
account_create_page.select_state()
account_create_page.enter_postcode("07097")
account_create_page.enter_cellphone("NotAPhone#")
account_create_page.enter_alias("Elm Street #888")

print("Click the [Submit] button...")
account_create_page.click_submit_button()

print("Waiting for the alert frame to show")
wait(10)

if driver.find_element(*AccountCreationPage_Locators.ALERT_FRAME).is_displayed():
    print("Test completed")

# All done --------------------------------------------------------

driver.quit()  # Close browser
quit()
sys.exit()
