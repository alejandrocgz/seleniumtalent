from selenium.webdriver.common.by import By


class MainPage_Locators:
    SIGN_IN_BUTTON = (By.CLASS_NAME, 'login')
    LOGOUT_BUTTON = (By.CLASS_NAME, 'logout')


class SignInPage_Locators:
    EMAIL_CREATE_INPUT_FIELD = (By.ID, 'email_create')
    CREATE_ACCOUNT_BTN = (By.ID, 'SubmitCreate')
    EMAIL_INPUT_FIELD = (By.ID, 'email')
    PASSWORD_INPUT_FIELD = (By.ID, 'passwd')
    SIGN_IN_BUTTON = (By.ID, 'SubmitLogin')


class AccountCreationPage_Locators:
    MR_RADIOBUTTON = (By.ID, 'id_gender1')
    MRS_RADIOBUTTON = (By.ID, 'id_gender2')
    FIRSTNAME = (By.ID, 'customer_firstname')
    LASTNAME = (By.ID, 'customer_lastname')
    PASSWORD = (By.ID, 'passwd')
    BIRTH_DAY = (By.CSS_SELECTOR, '#days > option:nth-child({day})'.format(day=26))
    BIRTH_MONTH = (By.CSS_SELECTOR, '#months > option:nth-child({month})'.format(month=9))
    BIRTH_YEAR = (By.CSS_SELECTOR, '#years > option:nth-child({year})'.format(year=27))
    NEWSLETTER_SUB = (By.NAME, 'newsletter')
    ADDRESS = (By.XPATH, '//*[@id="address1"]')
    CITY = (By.XPATH, '//*[@id="city"]')
    COUNTRY = (By.CSS_SELECTOR, '#id_country > option:nth-child(2)')
    STATE = (By.CSS_SELECTOR, '#id_state > option:nth-child(32)')
    POSTCODE = (By.CSS_SELECTOR, '#postcode')
    CELLPHONE = (By.XPATH, '//*[@id="phone_mobile"]')
    ALIAS = (By.NAME, 'alias')
    SUBMIT_BUTTON = (By.ID, 'submitAccount')
    ALERT_FRAME = (By.CSS_SELECTOR, '.alert')


class AccountPage_Locators:
    LOGOUT_BUTTON = (By.CLASS_NAME, 'logout')


class CartPage_Locators:
    CONTINUE_SHOPPING_BUTTON = (By.CSS_SELECTOR, '.button-exclusive')
    REMOVE_BLOUSE27 = (By.ID, '2_7_0_0')
    TOTAL = (By.ID, 'total_price')
    PROCEED_CHECKOUT = (By.CSS_SELECTOR, '.standard-checkout')
    CHECKBOX_DELIVERY_AS_BILLING_ADDRESS = (By.ID, 'addressesAreEquals')
    PROCESS_ADDRESS_BUTTON = (By.NAME, 'processAddress')
    CHECKBOX_TOS = (By.ID,'cgv')
    PROCESS_CARRIER_BUTTON = (By.NAME, 'processCarrier')
    PAY_BY_CHECK = (By.CSS_SELECTOR, '.cheque')
    CONFIRM_ORDER_BUTTON = (By.CSS_SELECTOR, 'button.button-medium')
    SUCCESS_ALERT = (By.CSS_SELECTOR, '.alert')
    ToS_ERROR = (By.CSS_SELECTOR, '.fancybox-error')