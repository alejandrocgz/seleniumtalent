import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import Page
import time


# Define a delayer function
def delayer(seconds, by):
    delay = seconds
    try:
        WebDriverWait(driver, delay).until(EC.presence_of_element_located(by))
        print("Delayer: The page is ready")
    except TimeoutException:
        print("Delayer: Loading took too much time")
        sys.exit()


def wait(seconds):
    delay = seconds
    WebDriverWait(driver, delay)


# Set the driver
driver = webdriver.Firefox()

# Required Page Objects
main_page = Page.MainPage(driver)
cart_page = Page.CartPage(driver)

# Webpage to navigate to:
i = 1
while i == 1:
    driver.get("http://automationpractice.com/index.php")
    wait(10)
    try:
        main_page.login_button_visible()
        i = 0
    except:
        print("Could not load the page... retrying in 15 seconds")
        wait(15)
        i = 1

# All set, its showtime! -------------------------------------------------------

print("Add the Blouse ($27) to he cart...")
main_page.blouse27_addToCart()
wait(10)

print("Click the [Continue shopping] button...")
cart_page.click_continue_shopping()

print("Add the Printed Summer Dress ($28.98) to he cart...")
main_page.pSummerDress28_addToCart()
wait(10)

print("Click the [Continue shopping] button...")
cart_page.click_continue_shopping()

print("Add the Chiffon Dress (#16.40) to he cart...")
main_page.pChiffonDress16_addToCart()
wait(10)

print("Reading the total value...")
total = cart_page.get_total()
print("Value: ", total)

if total == "$74.38":
    print("The total equals $74.38")
else:
    print("Something went wrong")
    sys.exit()

print("Removing Blouse ($27)")
cart_page.remove_Blouse27()

time.sleep(5)

print("Reading the new total value...")
total = cart_page.get_total()
print("Value: ", total)

if total == "$47.38":
    print("The total equals $47.38")
else:
    print("Something went wrong")
    sys.exit()

print("Test completed")

# All done --------------------------------------------------------

driver.quit()  # Close browser
quit()
sys.exit()
